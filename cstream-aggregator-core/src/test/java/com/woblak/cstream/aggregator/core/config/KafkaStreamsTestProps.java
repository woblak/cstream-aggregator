package com.woblak.cstream.aggregator.core.config;

import org.apache.kafka.streams.StreamsConfig;

import java.util.Properties;

public abstract class KafkaStreamsTestProps {

    public static Properties defaultStreamProps(String bootstrapServersConfig) {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "test_statefull_app_id");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServersConfig);
        props.put(StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG,
                "org.apache.kafka.streams.errors.LogAndContinueExceptionHandler");
        props.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 1 * 1042 * 1024L ); // 1 MB = 1 * 1042 * 1024L
        props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 200);
        return props;
    }
}
