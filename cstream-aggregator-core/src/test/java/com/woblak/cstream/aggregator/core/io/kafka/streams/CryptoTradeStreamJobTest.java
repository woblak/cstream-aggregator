package com.woblak.cstream.aggregator.core.io.kafka.streams;

import com.woblak.cstream.aggregator.api.event.MeanTradePrice;
import com.woblak.cstream.aggregator.api.event.SummaryTrade;
import com.woblak.cstream.aggregator.api.message.MeanTradePriceMsg;
import com.woblak.cstream.aggregator.api.message.SummaryTradeMsg;
import com.woblak.cstream.aggregator.core.config.EmbeddedKafkaTest;
import com.woblak.cstream.aggregator.core.config.KafkaStreamsTestProps;
import com.woblak.cstream.aggregator.core.io.kafka.streams.builder.KafkaStreamsBuilder;
import com.woblak.cstream.aggregator.core.io.kafka.streams.job.cryptotrade.CryptoTradeStreamJob;
import com.woblak.cstream.api.KafkaHeaders;
import com.woblak.cstream.api.KafkaTopics;
import com.woblak.cstream.ingester.api.event.CryptoTrade;
import com.woblak.cstream.ingester.api.message.CryptoTradeMsg;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.streams.KafkaStreams;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.annotation.DirtiesContext;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@Execution(ExecutionMode.SAME_THREAD)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
class CryptoTradeStreamJobTest extends EmbeddedKafkaTest {

    @Autowired
    CryptoTradeStreamJob cryptoTradeStreamJob;

    @Autowired
    @Qualifier("defaultStreamsProps")
    Properties defaultStreamsProperties;

    BlockingQueue<ConsumerRecord<String, Object>> records;

    @AfterEach
    void tearDown() {
        super.stop();
    }

    @Test
    void shouldAggregateSummary() {

        // given
        final String cryptoCode = "btc/usd";
        List<Message<CryptoTrade>> input = createCryptoTradeMsgList(cryptoCode, 1622718152, 1.0);

        // when
        setUp(KafkaTopics.CRYPTO_AGG_SUMMARY);
        input.forEach(super::sendMessage);

        // then
        await()
                .with()
                .alias("Wait until any message will be received.")
                .given()
                .atMost(20, TimeUnit.SECONDS)
                .pollInterval(400, TimeUnit.MILLISECONDS)
                .then()
                .until(() -> records.size() >= 4);

        Map<String, List<SummaryTrade>> summaryRecordsByWindow = records.stream()
                .map(record -> (SummaryTrade) record.value())
                .collect(Collectors.groupingBy(SummaryTrade::getWindowDurationName));

        SummaryTrade summaryTrade = summaryRecordsByWindow.get("PT1M")
                .get(summaryRecordsByWindow.get("PT1M").size() - 1);

        ConsumerRecord<String, Object> record = records.poll();

        assertEquals(cryptoCode, record.key());
        assertEquals(String.valueOf(record.timestamp()),
                new String(record.headers().headers(KafkaHeaders.LAST_TIMESTAMP).iterator().next().value()));
        assertEquals(SummaryTradeMsg.EVENT,
                new String(record.headers().lastHeader(KafkaHeaders.EVENT).value()));
        assertEquals(SummaryTradeMsg.VERSION,
                new String(record.headers().lastHeader(KafkaHeaders.VERSION).value()));
        assertEquals(1, summaryRecordsByWindow.get("PT1M").size());
        assertEquals(3L, summaryTrade.getData().getCount().getValue());
        assertEquals(10.0, summaryTrade.getData().getMinAmountQuote().getValue().doubleValue());
        assertEquals(90.0, summaryTrade.getData().getMaxAmountQuote().getValue().doubleValue());
        assertEquals(1.0, summaryTrade.getData().getMinPrice().getValue().doubleValue());
        assertEquals(3.0, summaryTrade.getData().getMaxPrice().getValue().doubleValue());
    }

    @Test
    void shouldAggregateMeanPrice() {

        // given
        final String cryptoCode = "btc/usd";
        List<Message<CryptoTrade>> input = createCryptoTradeMsgList(cryptoCode, 1622718152, 1.0);

        // when
        setUp(KafkaTopics.CRYPTO_AGG_MEAN);
        input.forEach(super::sendMessage);

        // then
        await()
                .with()
                .alias("Wait until any message will be received.")
                .given()
                .atMost(20, TimeUnit.SECONDS)
                .pollInterval(400, TimeUnit.MILLISECONDS)
                .then()
                .until(() -> records.size() >= 4);

        Map<String, List<MeanTradePrice>> meanTradePriceByWindow = records.stream()
                .map(record -> (MeanTradePrice) record.value())
                .collect(Collectors.groupingBy(MeanTradePrice::getWindowDurationName));

        MeanTradePrice meanTradePrice = meanTradePriceByWindow.get("PT1MPT15S")
                .get(meanTradePriceByWindow.get("PT1MPT15S").size() - 1);

        ConsumerRecord<String, Object> record = records.poll();

        assertEquals(cryptoCode, record.key());
        assertEquals(String.valueOf(record.timestamp()),
                new String(record.headers().lastHeader(KafkaHeaders.LAST_TIMESTAMP).value()));
        assertEquals(MeanTradePriceMsg.EVENT,
                new String(record.headers().lastHeader(KafkaHeaders.EVENT).value()));
        assertEquals(MeanTradePriceMsg.VERSION,
                new String(record.headers().lastHeader(KafkaHeaders.VERSION).value()));
        assertEquals(2.0, meanTradePrice.getData().getMean().doubleValue());
        assertEquals(2.34, meanTradePrice.getData().getMeanWagedByAmount().doubleValue());
        assertEquals(3.00, meanTradePrice.getData().getMaxPrice().getValue().doubleValue());
        assertEquals(1.00, meanTradePrice.getData().getMinPrice().getValue().doubleValue());
    }

    private List<Message<CryptoTrade>> createCryptoTradeMsgList(
            String code, long baseTimestamp, double basePrice
    ) {
        long timestamp = baseTimestamp;
        double price = basePrice;
        Message<CryptoTrade> msg1 = createCryptoTradeMsg(code, timestamp++, price++);
        Message<CryptoTrade> msg2 = createCryptoTradeMsg(code, timestamp++, price++);
        Message<CryptoTrade> msg3 = createCryptoTradeMsg(code, timestamp++, price++);
        return List.of(msg1, msg2, msg3);
    }

   private Message<CryptoTrade> createCryptoTradeMsg(String code, long timestamp, double price) {
       CryptoTrade payload = createCryptoTrade(code, timestamp, price);
        return MessageBuilder.withPayload(payload)
                .setHeader(KafkaHeaders.TOPIC, KafkaTopics.CRYPTO_TRADES)
                .setHeader(KafkaHeaders.MESSAGE_KEY, code)
                .setHeader(KafkaHeaders.EVENT, CryptoTradeMsg.EVENT)
                .setHeader(KafkaHeaders.VERSION, CryptoTradeMsg.VERSION)
                .build();
    }

    private CryptoTrade createCryptoTrade(String key, long timestamp, double price) {
        var market = createCryptoTradeMarket();
        var trade = craeteCryptoTradeTrade(timestamp, price);
        return CryptoTrade.builder()
                .uuid(UUID.randomUUID().toString())
                .eventTimestamp(timestamp)
                .code(key)
                .provider(key)
                .replicaNumber(0)
                .market(market)
                .trade(trade)
                .build();
    }

    private CryptoTrade.Market createCryptoTradeMarket() {
        return CryptoTrade.Market.builder()
                .currencyPairId("currencyPairId")
                .exchangeId("exchangeId")
                .marketId("marketId")
                .build();
    }

    private CryptoTrade.Trade craeteCryptoTradeTrade(long timestamp, double price) {
        BigDecimal priceDecimal = BigDecimal.valueOf(price);
        BigDecimal amount = BigDecimal.TEN.multiply(priceDecimal);
        BigDecimal amountQuote = amount.multiply(priceDecimal);
        return CryptoTrade.Trade.builder()
                .timestampNano(timestamp)
                .amount(amount)
                .amountQuote(amountQuote)
                .price(priceDecimal)
                .externalId("externalId")
                .orderSide("seller")
                .build();
    }

    private void setUp(String aggTopic) {
        this.records = new ArrayBlockingQueue<>(64);

        setUpEmbeddedKafka(aggTopic);

        super.start();

        KafkaStreams streams = KafkaStreamsBuilder
                .withProps(KafkaStreamsTestProps.defaultStreamProps("localhost:" + super.port))
                .addJob(cryptoTradeStreamJob)
                .build();
        super.startKafkaStreams(streams);
    }

    private void setUpEmbeddedKafka(String aggTopic) {
        super.setTopics(KafkaTopics.CRYPTO_TRADES, aggTopic);
        super.addListener(record -> {
            if (aggTopic.equals(record.topic())) {
                records.add(record);
            }
        });
    }
}
