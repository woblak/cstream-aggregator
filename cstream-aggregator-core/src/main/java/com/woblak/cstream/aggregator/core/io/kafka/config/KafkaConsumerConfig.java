package com.woblak.cstream.aggregator.core.io.kafka.config;


import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ListenerUtils;
import org.springframework.kafka.listener.RecordInterceptor;
import org.springframework.kafka.listener.adapter.RecordFilterStrategy;
import org.springframework.kafka.support.DefaultKafkaHeaderMapper;
import org.springframework.kafka.support.serializer.ErrorHandlingDeserializer;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@Profile({"metrics", "prod", "dev"})
@EnableKafka
@Configuration
@Slf4j
public class KafkaConsumerConfig {

    private final String bootstrapAddress;
    private final String groupId;
    private final String autoOffsetReset;
    private final boolean ackDiscarded;

    public KafkaConsumerConfig(
            @Value("${spring.kafka.bootstrap-servers}") String bootstrapAddress,
            @Value("${spring.kafka.consumer.group-id}") String groupId,
            @Value("${spring.kafka.consumer.auto-offset-reset}") String autoOffsetReset,
            @Value("${spring.kafka.consumer.ack-discarded:true}") boolean ackDiscarded
    ) {
        this.bootstrapAddress = bootstrapAddress;
        this.groupId = groupId;
        this.autoOffsetReset = autoOffsetReset;
        this.ackDiscarded = ackDiscarded;
    }

    @Bean("kafkaListenerContainerFactory")
    public ConcurrentKafkaListenerContainerFactory<String, Object> kafkaListenerContainerFactory(
            ConsumerFactory<String, Object> consumerFactory,
            RecordInterceptor<String, Object> recordInterceptor,
            RecordFilterStrategy<String, Object> recordFilterStrategy
    ) {
        ConcurrentKafkaListenerContainerFactory<String, Object> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory);
        factory.setRecordInterceptor(recordInterceptor);
        factory.setAckDiscarded(ackDiscarded);
        factory.setRecordFilterStrategy(recordFilterStrategy);
        return factory;
    }

    @Bean
    public ConsumerFactory<String, Object> consumerFactory(
            @Qualifier("consumerKeyDeserializer") Deserializer<String> keyDeserializer,
            @Qualifier("consumerValueDeserializer") Deserializer<Object> valueDeserializer
    ) {
        return new DefaultKafkaConsumerFactory<>(consumerConfigs(), keyDeserializer, valueDeserializer);
    }

    @Bean
    public Map<String, Object> consumerConfigs() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        configs.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        configs.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, autoOffsetReset);
        return configs;
    }

    @Bean
    public Deserializer<String> consumerKeyDeserializer() {
        return new ErrorHandlingDeserializer<>(new StringDeserializer());
    }

    @Bean
    public Deserializer<Object> consumerValueDeserializer() {
        JsonDeserializer<Object> jsonDeserializer = new JsonDeserializer<>();
        jsonDeserializer.addTrustedPackages("*");
        return new ErrorHandlingDeserializer<>(jsonDeserializer);
    }

    @Bean
    public DefaultKafkaHeaderMapper kafkaHeaderMapper() {
        return new DefaultKafkaHeaderMapper();
    }

    @Bean
    public RecordInterceptor<String, Object> recordInterceptor() {
        return consumerRecord -> {
            String str = ListenerUtils.recordToString(consumerRecord);
            log.info("<< kafka << {}", str);
            return consumerRecord;
        };
    }

    @Bean
    RecordFilterStrategy<String, Object> recordFilterStrategy() {
        return record -> {
            return false;
        };
    }

}