package com.woblak.cstream.aggregator.core.io.kafka.streams.job.cryptotrade;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.time.Duration;
import java.util.List;

@Configuration
class Config {

    @Bean
    List<Duration> summaryDurations() {
        return List.of(
                Duration.ofMinutes(1),
                Duration.ofMinutes(10),
                Duration.ofMinutes(60),
                Duration.ofHours(24));
    }

    @Bean
    List<MeanPriceStream.DurationWithAdvance> meanPriceDurationsWithAdvance() {
        return List.of(
                MeanPriceStream.DurationWithAdvance.of(Duration.ofMinutes(1), Duration.ofSeconds(15)),
                MeanPriceStream.DurationWithAdvance.of(Duration.ofMinutes(10), Duration.ofMinutes(1)),
                MeanPriceStream.DurationWithAdvance.of(Duration.ofMinutes(60), Duration.ofMinutes(10)),
                MeanPriceStream.DurationWithAdvance.of(Duration.ofHours(24), Duration.ofHours(1)));
    }
}
