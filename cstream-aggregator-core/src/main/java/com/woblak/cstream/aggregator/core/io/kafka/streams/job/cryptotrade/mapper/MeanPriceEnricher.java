package com.woblak.cstream.aggregator.core.io.kafka.streams.job.cryptotrade.mapper;

import com.woblak.cstream.aggregator.api.event.MeanTradePrice;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KeyValueMapper;
import org.apache.kafka.streams.kstream.Windowed;
import org.springframework.stereotype.Component;

@Component
public class MeanPriceEnricher
        implements KeyValueMapper<Windowed<String>, MeanTradePrice, KeyValue<String, MeanTradePrice>> {

    @Override
    public KeyValue<String, MeanTradePrice> apply(Windowed<String> k, MeanTradePrice v) {
        var metaData = MeanTradePrice.MetaData.builder()
                .startMs(k.window().start())
                .endMs(k.window().end())
                .build();
        var summaryTrade = MeanTradePrice.builder()
                .code(v.getCode())
                .windowDurationName(v.getWindowDurationName())
                .metaData(metaData)
                .data(v.getData())
                .build();
        return KeyValue.pair(k.key(), summaryTrade);
    }
}
