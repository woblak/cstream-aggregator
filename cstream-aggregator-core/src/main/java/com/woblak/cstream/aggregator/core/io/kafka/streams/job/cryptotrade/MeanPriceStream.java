package com.woblak.cstream.aggregator.core.io.kafka.streams.job.cryptotrade;

import com.woblak.cstream.aggregator.api.event.MeanTradePrice;
import com.woblak.cstream.aggregator.api.message.MeanTradePriceMsg;
import com.woblak.cstream.aggregator.core.io.kafka.streams.job.WindowedStream;
import com.woblak.cstream.aggregator.core.io.kafka.streams.job.cryptotrade.aggregators.MeanPriceAggregator;
import com.woblak.cstream.aggregator.core.io.kafka.streams.job.cryptotrade.mapper.MeanPriceEnricher;
import com.woblak.cstream.aggregator.core.io.kafka.streams.job.cryptotrade.transformer.HeadersTransformer;
import com.woblak.cstream.ingester.api.event.CryptoTrade;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.Windowed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;

@Component("meanPriceJob")
public
class MeanPriceStream implements WindowedStream<String, CryptoTrade, String, MeanTradePrice> {

    private final MeanPriceEnricher meanPriceEnricher;
    private final List<DurationWithAdvance> meanPriceDurationsWithAdvances;

    private final Serde<String> stringSerde;
    private final Serde<MeanTradePrice> meanTradePriceSerde;

    private final boolean shouldSuppressUntilWindowCloses;

    @Autowired
    public MeanPriceStream(
            MeanPriceEnricher meanPriceEnricher,
            List<DurationWithAdvance> meanPriceDurationsWithAdvances,
            Serde<String> stringSerde,
            Serde<MeanTradePrice> meanTradePriceSerde,
            @Value("${spring.kafka.streams.suppressUntilWindowCloses : false}") boolean shouldSuppressUntilWindowCloses
    ) {
        this.meanPriceEnricher = meanPriceEnricher;
        this.meanPriceDurationsWithAdvances = meanPriceDurationsWithAdvances;
        this.stringSerde = stringSerde;
        this.meanTradePriceSerde = meanTradePriceSerde;
        this.shouldSuppressUntilWindowCloses = shouldSuppressUntilWindowCloses;
    }

    @Override
    public List<KStream<String, MeanTradePrice>> streamWindowed(KStream<String, CryptoTrade> kStream) {
        return meanPriceDurationsWithAdvances.stream()
                .map(d -> aggregateMean(kStream, d.getDuration(), d.getAdvance()))
                .map(KTable::toStream)
                .map(i -> i.map(meanPriceEnricher))
                .map(i -> i.transform(() -> new HeadersTransformer<>(MeanTradePriceMsg.EVENT, MeanTradePriceMsg.VERSION)))
                .collect(Collectors.toList());
    }

    private KTable<Windowed<String>, MeanTradePrice> aggregateMean(
            KStream<String, CryptoTrade> tradesStream,
            Duration duration,
            Duration advance
    ) {
        String windowDurationName = duration.toString() + advance.toString();
        return tradesStream.groupByKey()
                .windowedBy(TimeWindows.of(duration).advanceBy(advance))
                .aggregate(
                        MeanTradePrice::init,
                        new MeanPriceAggregator(windowDurationName),
                        Materialized.with(stringSerde, meanTradePriceSerde));
    }

    @RequiredArgsConstructor
    @Getter
    public static class DurationWithAdvance {

        public static DurationWithAdvance of(Duration duration, Duration advance) {
            return new DurationWithAdvance(duration, advance);
        }

        private final Duration duration;
        private final Duration advance;
    }
}
