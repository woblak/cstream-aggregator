package com.woblak.cstream.aggregator.core.io.kafka.config;

import com.woblak.cstream.aggregator.api.event.MeanTradePrice;
import com.woblak.cstream.aggregator.api.event.SummaryTrade;
import com.woblak.cstream.aggregator.core.util.SerdeBuilder;
import com.woblak.cstream.ingester.api.event.CryptoTrade;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SerdesConfig {

    @Bean
    Serde<String> stringSerde() {
        return Serdes.String();
    }

    @Bean
    Serde<MeanTradePrice> meanTradePriceSerde() {
        return SerdeBuilder.create(MeanTradePrice.class);
    }

    @Bean
    Serde<SummaryTrade> summaryTradeSerde() {
        return SerdeBuilder.create(SummaryTrade.class);
    }

    @Bean
    Serde<CryptoTrade> cryptoTradeSerde() {
        return SerdeBuilder.create(CryptoTrade.class);
    }
}
