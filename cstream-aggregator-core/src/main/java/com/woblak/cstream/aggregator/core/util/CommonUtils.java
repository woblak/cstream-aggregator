package com.woblak.cstream.aggregator.core.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class CommonUtils {

    public static String truncateString(String s, int limit) {
        return s != null && limit >= 0 && s.length() > limit ? truncate(s, limit) : s;
    }

    private String truncate(String s, int limit) {
        return s.substring(0, limit ) + " ...";
    }
}
