package com.woblak.cstream.aggregator.core.io.kafka.config;


import com.woblak.cstream.api.KafkaTopics;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaAdmin;

import java.util.HashMap;
import java.util.Map;

@Profile({"metrics", "prod", "dev"})
@Configuration
@Slf4j
public class KafkaTopicConfig {

    private final String bootstrapAddress;
    private final int cryptoAggSummaryTopicPartitions;
    private final int cryptoAggSummaryTopicReplicas;
    private final int cryptoAggMeanTopicPartitions;
    private final int cryptoAggMeanTopicReplicas;

    public KafkaTopicConfig(
            @Value("${spring.kafka.bootstrap-servers}") String bootstrapAddress,
            @Value("${spring.kafka.topics.crypto-agg-summary.partitions:1}") int cryptoAggSummaryTopicPartitions,
            @Value("${spring.kafka.topics.crypto-agg-summary.replicas:1}") int cryptoAggSummaryTopicReplicas,
            @Value("${spring.kafka.topics.crypto-agg-mean.partitions:1}") int cryptoAggMeanTopicPartitions,
            @Value("${spring.kafka.topics.crypto-agg-mean.replicas:1}") int cryptoAggMeanTopicReplicas

    ) {
        this.bootstrapAddress = bootstrapAddress;
        this.cryptoAggSummaryTopicPartitions = cryptoAggSummaryTopicPartitions;
        this.cryptoAggSummaryTopicReplicas = cryptoAggSummaryTopicReplicas;
        this.cryptoAggMeanTopicPartitions = cryptoAggMeanTopicPartitions;
        this.cryptoAggMeanTopicReplicas = cryptoAggMeanTopicReplicas;
    }

    @Bean
    public NewTopic cryptoAggSummaryTopic() {
        return TopicBuilder.name(KafkaTopics.CRYPTO_AGG_SUMMARY)
                .partitions(cryptoAggSummaryTopicPartitions)
                .replicas(cryptoAggSummaryTopicReplicas)
                .build();
    }

    @Bean
    public NewTopic cryptoAggMeanTopic() {
        return TopicBuilder.name(KafkaTopics.CRYPTO_AGG_MEAN)
                .partitions(cryptoAggMeanTopicPartitions)
                .replicas(cryptoAggMeanTopicReplicas)
                .build();
    }

    @Bean
    public KafkaAdmin kafkaAdmin() {
        Map<String, Object> configs = topicConfigs();
        return new KafkaAdmin(configs);
    }

    @Bean
    public Map<String, Object> topicConfigs() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        return configs;
    }

    @Bean
    public Map<String, Object> topicConfigs(
            @Value("${spring.kafka.bootstrap-servers}") String bootstrapAddress
    ) {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        return configs;
    }
}