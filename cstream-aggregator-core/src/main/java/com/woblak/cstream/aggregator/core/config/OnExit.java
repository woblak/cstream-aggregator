package com.woblak.cstream.aggregator.core.config;

import lombok.experimental.UtilityClass;

@UtilityClass
public class OnExit {

    public static void addShutdownHook(Runnable runnable) {
        Runtime.getRuntime().addShutdownHook(new Thread(runnable));
    }
}
