package com.woblak.cstream.aggregator.core.io.kafka.streams.job.cryptotrade;

import com.woblak.cstream.aggregator.api.event.SummaryTrade;
import com.woblak.cstream.aggregator.api.message.SummaryTradeMsg;
import com.woblak.cstream.aggregator.core.io.kafka.streams.job.cryptotrade.aggregators.SummaryTradeAggregator;
import com.woblak.cstream.aggregator.core.io.kafka.streams.job.WindowedStream;
import com.woblak.cstream.aggregator.core.io.kafka.streams.job.cryptotrade.mapper.SummaryTradeEnricher;
import com.woblak.cstream.aggregator.core.io.kafka.streams.job.cryptotrade.transformer.HeadersTransformer;
import com.woblak.cstream.ingester.api.event.CryptoTrade;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.Windowed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;

@Component("summaryJob")
class SummaryStream implements WindowedStream<String, CryptoTrade, String, SummaryTrade> {

    private final SummaryTradeEnricher summaryTradeEnricher;
    private final List<Duration> summaryDurations;

    private final Serde<String> stringSerde;
    private final Serde<SummaryTrade> summaryTradeSerde;

    private final boolean shouldSuppressUntilWindowCloses;

    @Autowired
    public SummaryStream(
            SummaryTradeEnricher summaryTradeEnricher,
            List<Duration> summaryDurations,
            Serde<String> stringSerde,
            Serde<SummaryTrade> summaryTradeSerde,
            @Value("${spring.kafka.streams.suppressUntilWindowCloses : false}") boolean shouldSuppressUntilWindowCloses) {
        this.summaryTradeEnricher = summaryTradeEnricher;
        this.summaryDurations = summaryDurations;
        this.stringSerde = stringSerde;
        this.summaryTradeSerde = summaryTradeSerde;
        this.shouldSuppressUntilWindowCloses = shouldSuppressUntilWindowCloses;
    }

    @Override
    public List<KStream<String, SummaryTrade>> streamWindowed(KStream<String, CryptoTrade> kStream) {
        return summaryDurations.stream()
                .map(d -> aggregateSummary(kStream, d))
                .map(KTable::toStream)
                .map(i -> i.map(summaryTradeEnricher))
                .map(i -> i.transform(() -> new HeadersTransformer<>(SummaryTradeMsg.EVENT, SummaryTradeMsg.VERSION)))
                .collect(Collectors.toList());
    }

    private KTable<Windowed<String>, SummaryTrade> aggregateSummary(
            KStream<String, CryptoTrade> tradesStream,
            Duration duration
    ) {
        return tradesStream.groupByKey()
                .windowedBy(TimeWindows.of(duration))
                .aggregate(
                        () -> null,
                        new SummaryTradeAggregator(duration.toString()),
                        Materialized.with(stringSerde, summaryTradeSerde));
    }
}
