package com.woblak.cstream.aggregator.core.util;

import lombok.experimental.UtilityClass;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

@UtilityClass
public class SerializerUtils {

    public static byte[] stringToByteArray(String s) {
        byte[] bytes;

        try (
                var baos = new ByteArrayOutputStream();
                var out = new ObjectOutputStream(baos)
        ) {
            out.writeObject(s);
            out.flush();
            bytes = baos.toByteArray();
        } catch (IOException e) {
            throw new UnsupportedOperationException(e);
        }

        return bytes;
    }
}
