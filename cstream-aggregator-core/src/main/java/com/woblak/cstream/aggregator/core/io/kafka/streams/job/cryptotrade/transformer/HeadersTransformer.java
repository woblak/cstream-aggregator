package com.woblak.cstream.aggregator.core.io.kafka.streams.job.cryptotrade.transformer;

import com.woblak.cstream.api.KafkaHeaders;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;

@RequiredArgsConstructor
public class HeadersTransformer<K, V> implements Transformer<K, V, KeyValue<K, V>> {

    private final String event;
    private final String version;

    ProcessorContext context;

    @Override
    public void init(ProcessorContext processorContext) {
        this.context = processorContext;
    }

    @Override
    public KeyValue<K, V> transform(K s, V cryptoTrade) {
        context.headers()
                .remove(KafkaHeaders.LAST_TIMESTAMP)
                .add(KafkaHeaders.LAST_TIMESTAMP, String.valueOf(context.timestamp()).getBytes())
                .remove(KafkaHeaders.EVENT)
                .add(KafkaHeaders.EVENT, event.getBytes())
                .remove(KafkaHeaders.VERSION)
                .add(KafkaHeaders.VERSION, version.getBytes());
        return KeyValue.pair(s, cryptoTrade);
    }

    @Override
    public void close() {

    }
}
