package com.woblak.cstream.aggregator.core.io.kafka.streams.job;

import org.apache.kafka.streams.StreamsBuilder;

public interface KafkaStreamJob {

    StreamsBuilder registerOn(StreamsBuilder streamsBuilder);
}
