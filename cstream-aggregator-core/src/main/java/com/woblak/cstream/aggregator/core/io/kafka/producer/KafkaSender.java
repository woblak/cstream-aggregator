package com.woblak.cstream.aggregator.core.io.kafka.producer;

import com.woblak.cstream.aggregator.core.config.AppProps;
import com.woblak.cstream.aggregator.core.exception.KafkaSendingFailedException;
import com.woblak.cstream.aggregator.core.util.CommonUtils;
import com.woblak.cstream.api.KafkaHeaders;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaProducerException;
import org.springframework.kafka.core.KafkaSendCallback;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class KafkaSender {

    private final KafkaTemplate<String, Object> kafkaTemplate;
    private final AppProps appProps;

    public ListenableFuture<SendResult<String, Object>> send(Message<?> msg) {
        log.debug(createLogMsg("Sending", msg));
        ListenableFuture<SendResult<String, Object>> future = kafkaTemplate.send(msg);
        future.addCallback(getCallback(msg));
        return future;
    }

    private KafkaSendCallback<String, Object> getCallback(Message<?> msg) {
        return new KafkaSendCallback<>() {
            @Override
            public void onSuccess(SendResult<String, Object> objectObjectSendResult) {
                log.info(createLogMsg("Successfuly sent", msg));
            }

            @Override
            public void onFailure(KafkaProducerException e) {
                log.error(createLogMsg("Failed sending", msg));
                log.error(e.getMessage());
                throw new KafkaSendingFailedException(e);
            }
        };
    }

    private String createLogMsg(String part, Message<?> msg) {
        MessageHeaders headers = msg.getHeaders();
        String payload = CommonUtils.truncateString(msg.getPayload().toString(), appProps.getPayloadMaxLength());
        return String.format(">> kafka >> %s [%s] to topic [%s] with headers: [%s] and payload: [%s]",
                part, headers.get("event"), headers.get(KafkaHeaders.TOPIC), headers, payload);
    }
}
