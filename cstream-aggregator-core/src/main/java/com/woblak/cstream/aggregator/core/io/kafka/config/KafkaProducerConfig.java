package com.woblak.cstream.aggregator.core.io.kafka.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@Profile({"metrics", "prod", "dev"})
@Configuration
@Slf4j
public class KafkaProducerConfig {

    private final String bootstrapAddress;
    private final String acks;
    private final String enableIdempotence;
    private final boolean autoFlush;

    public KafkaProducerConfig(
            @Value("${spring.kafka.bootstrap-servers}") String bootstrapAddress,
            @Value("${spring.kafka.producer.acks}") String acks,
            @Value("${spring.kafka.producer.enable.idempotence}") String enableIdempotence,
            @Value("${spring.kafka.producer.autoFlush:true}") boolean autoFlush) {
        this.bootstrapAddress = bootstrapAddress;
        this.acks = acks;
        this.enableIdempotence = enableIdempotence;
        this.autoFlush = autoFlush;
    }

    @Bean
    public KafkaTemplate<String, Object> kafkaTemplate(
            ProducerFactory<String, Object> producerFactory
    ) {
        return new KafkaTemplate<>(producerFactory, autoFlush);
    }

    @Bean
    public ProducerFactory<String, Object> producerFactory(
            @Qualifier("producerKeySerializer") Serializer<String> producerKeySerializer,
            @Qualifier("producerValueSerializer") Serializer<Object> producerValueSerializer
    ) {
        return new DefaultKafkaProducerFactory<>(producerConfigs(), producerKeySerializer, producerValueSerializer);
    }

    public Map<String, Object> producerConfigs() {
        return producerConfigs(bootstrapAddress);
    }

    public Map<String, Object> producerConfigs(String producerBootstrapAddress) {
        Map<String, Object> configs = new HashMap<>();
        configs.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, producerBootstrapAddress);
        configs.put(ProducerConfig.ACKS_CONFIG, acks);
        configs.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, enableIdempotence);
        configs.put(JsonDeserializer.TRUSTED_PACKAGES, "*");
        return configs;
    }

    @Bean
    public Serializer<String> producerKeySerializer() {
        return new StringSerializer();
    }

    @Bean
    public Serializer<Object> producerValueSerializer() {
        return new JsonSerializer<>();
    }
}
