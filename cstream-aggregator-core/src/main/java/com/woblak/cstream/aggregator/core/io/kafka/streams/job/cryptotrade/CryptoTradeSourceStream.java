package com.woblak.cstream.aggregator.core.io.kafka.streams.job.cryptotrade;

import com.woblak.cstream.aggregator.core.io.kafka.streams.job.SourceStream;
import com.woblak.cstream.aggregator.core.io.kafka.streams.job.cryptotrade.extractor.AggTimestampExtractor;
import com.woblak.cstream.api.KafkaTopics;
import com.woblak.cstream.ingester.api.event.CryptoTrade;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile({"!metrics"})
@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class CryptoTradeSourceStream implements SourceStream<String, CryptoTrade> {

    private final Serde<String> stringSerde;
    private final Serde<CryptoTrade> cryptoTradeSerde;
    private final AggTimestampExtractor aggTimestampExtractor;

    @Override
    public KStream<String, CryptoTrade> streamSource(StreamsBuilder sb) {
        return sb.stream(KafkaTopics.CRYPTO_TRADES, Consumed.with(stringSerde, cryptoTradeSerde)
                .withTimestampExtractor(aggTimestampExtractor));
    }
}
