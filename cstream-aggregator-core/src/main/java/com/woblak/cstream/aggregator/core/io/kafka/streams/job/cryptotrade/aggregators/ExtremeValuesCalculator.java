package com.woblak.cstream.aggregator.core.io.kafka.streams.job.cryptotrade.aggregators;

import com.woblak.cstream.aggregator.api.event.AggregatedValue;
import com.woblak.cstream.ingester.api.event.CryptoTrade;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.function.Function;

@Component
class ExtremeValuesCalculator<T> {

    AggregatedValue<BigDecimal> calculateExtremeValueRecord(
            CryptoTrade v,
            T agg,
            Function<CryptoTrade, BigDecimal> vGetter,
            Function<T, AggregatedValue<BigDecimal>> aggGetter,
            boolean isMax
    ) {
        boolean shouldReplaceAgg = agg == null ||
                aggGetter.apply(agg) == null ||
                (isMax ?
                        vGetter.apply(v).compareTo(aggGetter.apply(agg).getValue()) > 0 :
                        vGetter.apply(v).compareTo(aggGetter.apply(agg).getValue()) < 0);

        return shouldReplaceAgg ?
                AggregatedValue.<BigDecimal>builder()
                        .uuid(v.getUuid())
                        .timestamp(v.getEventTimestamp())
                        .value(vGetter.apply(v))
                        .build() :
                aggGetter.apply(agg);
    }
}
