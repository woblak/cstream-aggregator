package com.woblak.cstream.aggregator.core.io.kafka.streams.job.cryptotrade.extractor;

import com.woblak.cstream.ingester.api.event.CryptoTrade;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.streams.processor.TimestampExtractor;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class AggTimestampExtractor implements TimestampExtractor {

    @Override
    public long extract(ConsumerRecord<Object, Object> consumerRecord, long l) {
        var record = (CryptoTrade) consumerRecord.value();
        return record.getEventTimestamp();
    }
}

