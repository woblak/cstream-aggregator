package com.woblak.cstream.aggregator.core.io.kafka.streams.job.cryptotrade.aggregators;

import com.woblak.cstream.aggregator.api.event.AggregatedValue;
import com.woblak.cstream.aggregator.api.event.MeanTradePrice;
import com.woblak.cstream.ingester.api.event.CryptoTrade;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.streams.kstream.Aggregator;
import org.springframework.lang.NonNull;

import java.math.BigDecimal;
import java.math.RoundingMode;

@RequiredArgsConstructor
public class MeanPriceAggregator implements Aggregator<String, CryptoTrade, MeanTradePrice> {

    private final String windowDurationName;
    private final ExtremeValuesCalculator<MeanTradePrice> extremeValuesCalculator = new ExtremeValuesCalculator<>();

    @Override
    public MeanTradePrice apply(String k, CryptoTrade v, @NonNull MeanTradePrice agg) {
        CryptoTrade.Trade trade = v.getTrade();
        long count = agg.getData().getCount() + 1;
        BigDecimal amountSum = agg.getData().getAmountSum().add(trade.getAmount());
        BigDecimal price = trade.getPrice();
        BigDecimal priceSum = agg.getData().getPriceSum().add(price);
        BigDecimal priceWagedByAmountSum = agg.getData().getPriceWagedByAmountSum().add(price.multiply(trade.getAmount()));
        BigDecimal mean = priceSum.divide(BigDecimal.valueOf(count), RoundingMode.CEILING);
        BigDecimal meanWagedByAmount = priceWagedByAmountSum.divide(amountSum, RoundingMode.CEILING);
        AggregatedValue<BigDecimal> maxPrice = calculateMaxPrice(v, agg);
        AggregatedValue<BigDecimal> minPrice = calculateMinPrice(v, agg);

        var data = MeanTradePrice.Data.builder()
                .count(count)
                .amountSum(amountSum)
                .priceSum(priceSum)
                .priceWagedByAmountSum(priceWagedByAmountSum)
                .mean(mean)
                .meanWagedByAmount(meanWagedByAmount)
                .maxPrice(maxPrice)
                .minPrice(minPrice)
                .build();
        return MeanTradePrice.builder()
                .code(k)
                .windowDurationName(windowDurationName)
                .data(data)
                .build();
    }

    private AggregatedValue<BigDecimal> calculateMaxPrice(CryptoTrade v, MeanTradePrice agg) {
        return extremeValuesCalculator.calculateExtremeValueRecord(v, agg, x -> x.getTrade().getPrice(),
                x -> x.getData().getMaxPrice(), true);
    }

    private AggregatedValue<BigDecimal> calculateMinPrice(CryptoTrade v, MeanTradePrice agg) {
        return extremeValuesCalculator.calculateExtremeValueRecord(v, agg, x -> x.getTrade().getPrice(),
                x -> x.getData().getMinPrice(), false);
    }
}
