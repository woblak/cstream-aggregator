package com.woblak.cstream.aggregator.core.io.kafka.streams.job;

import org.apache.kafka.streams.kstream.KStream;

public interface ProcessingStream<K, V> {

    void stream(KStream<K, V> kStream);
}
