package com.woblak.cstream.aggregator.core.io.kafka.streams.job.cryptotrade;

import com.woblak.cstream.aggregator.api.event.MeanTradePrice;
import com.woblak.cstream.aggregator.api.event.SummaryTrade;
import com.woblak.cstream.aggregator.core.io.kafka.streams.job.KafkaStreamJob;
import com.woblak.cstream.aggregator.core.io.kafka.streams.job.SourceStream;
import com.woblak.cstream.aggregator.core.io.kafka.streams.job.WindowedStream;
import com.woblak.cstream.api.KafkaTopics;
import com.woblak.cstream.ingester.api.event.CryptoTrade;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Printed;
import org.apache.kafka.streams.kstream.Produced;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class CryptoTradeStreamJob implements KafkaStreamJob {

    private final SourceStream<String, CryptoTrade> sourceStream;

    @Qualifier("summaryJob")
    private final WindowedStream<String, CryptoTrade, String, SummaryTrade> summaryJob;

    @Qualifier("meanPriceJob")
    private final WindowedStream<String, CryptoTrade, String, MeanTradePrice> meanPriceJob;

    private final Serde<String> stringSerde;
    private final Serde<SummaryTrade> summaryTradeSerde;
    private final Serde<MeanTradePrice> meanTradePriceSerde;

    @Override
    public StreamsBuilder registerOn(StreamsBuilder sb) {

        KStream<String, CryptoTrade> tradeSource = sourceStream.streamSource(sb);

        List<KStream<String, SummaryTrade>> summaryStreams = summaryJob.streamWindowed(tradeSource);
        summaryStreams.forEach(stream -> {
            stream.print(Printed.toSysOut());
            stream.to(KafkaTopics.CRYPTO_AGG_SUMMARY, Produced.with(stringSerde, summaryTradeSerde));
        });

        List<KStream<String, MeanTradePrice>> meanPriceStreams = meanPriceJob.streamWindowed(tradeSource);
        meanPriceStreams.forEach(stream -> {
            stream.print(Printed.toSysOut());
            stream.to(KafkaTopics.CRYPTO_AGG_MEAN, Produced.with(stringSerde, meanTradePriceSerde));
        });

        return sb;
    }
}
