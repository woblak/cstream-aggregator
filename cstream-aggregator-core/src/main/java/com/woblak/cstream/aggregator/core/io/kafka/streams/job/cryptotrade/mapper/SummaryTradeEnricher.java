package com.woblak.cstream.aggregator.core.io.kafka.streams.job.cryptotrade.mapper;

import com.woblak.cstream.aggregator.api.event.SummaryTrade;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KeyValueMapper;
import org.apache.kafka.streams.kstream.Windowed;
import org.springframework.stereotype.Component;

@Component
public class SummaryTradeEnricher
        implements KeyValueMapper<Windowed<String>, SummaryTrade, KeyValue<String, SummaryTrade>> {

    @Override
    public KeyValue<String, SummaryTrade> apply(Windowed<String> k, SummaryTrade v) {
        var metaData = SummaryTrade.MetaData.builder()
                .startMs(k.window().start())
                .endMs(k.window().end())
                .build();
        var summaryTrade = SummaryTrade.builder()
                .code(v.getCode())
                .windowDurationName(v.getWindowDurationName())
                .metaData(metaData)
                .data(v.getData())
                .build();
        return KeyValue.pair(k.key(), summaryTrade);
    }
}
