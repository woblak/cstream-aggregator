package com.woblak.cstream.aggregator.core.io.kafka.streams.job.cryptotrade.aggregators;

import com.woblak.cstream.aggregator.api.event.AggregatedValue;
import com.woblak.cstream.aggregator.api.event.SummaryTrade;
import com.woblak.cstream.ingester.api.event.CryptoTrade;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.streams.kstream.Aggregator;
import org.springframework.lang.Nullable;

import java.math.BigDecimal;

@RequiredArgsConstructor
public class SummaryTradeAggregator implements Aggregator<String, CryptoTrade, SummaryTrade> {

    private final String windowDurationName;
    private final ExtremeValuesCalculator<SummaryTrade> extremeValuesCalculator = new ExtremeValuesCalculator<>();

    @Override
    public SummaryTrade apply(String k, CryptoTrade v, @Nullable SummaryTrade agg) {
        AggregatedValue<Long> count = calculateCount(v, agg);
        AggregatedValue<BigDecimal> maxAmountQuote = calculateMaxAmountQuote(v, agg);
        AggregatedValue<BigDecimal> minAmountQuote = calculateMinAmountQuote(v, agg);
        AggregatedValue<BigDecimal> maxPrice = calculateMaxPrice(v, agg);
        AggregatedValue<BigDecimal> minPrice = calculateMinPrice(v, agg);

        var data = SummaryTrade.Data
                .builder()
                .count(count)
                .maxAmountQuote(maxAmountQuote)
                .minAmountQuote(minAmountQuote)
                .maxPrice(maxPrice)
                .minPrice(minPrice)
                .build();
        return SummaryTrade.builder()
                .code(k)
                .windowDurationName(windowDurationName)
                .data(data)
                .build();
    }

    private AggregatedValue<Long> calculateCount(CryptoTrade v, SummaryTrade agg) {
        long currentCount = agg == null ? 0L : agg.getData().getCount().getValue();
        return AggregatedValue.<Long>builder()
                .uuid(v.getUuid())
                .timestamp(v.getEventTimestamp())
                .value(currentCount + 1)
                .build();
    }

    private AggregatedValue<BigDecimal> calculateMaxAmountQuote(CryptoTrade v, SummaryTrade agg) {
        return extremeValuesCalculator.calculateExtremeValueRecord(v, agg, x -> x.getTrade().getAmountQuote(),
                x -> x.getData().getMaxAmountQuote(), true);
    }

    private AggregatedValue<BigDecimal> calculateMinAmountQuote(CryptoTrade v, SummaryTrade agg) {
        return extremeValuesCalculator.calculateExtremeValueRecord(v, agg, x -> x.getTrade().getAmountQuote(),
                x -> x.getData().getMinAmountQuote(), false);
    }

    private AggregatedValue<BigDecimal> calculateMaxPrice(CryptoTrade v, SummaryTrade agg) {
        return extremeValuesCalculator.calculateExtremeValueRecord(v, agg, x -> x.getTrade().getPrice(),
                x -> x.getData().getMaxPrice(), true);
    }

    private AggregatedValue<BigDecimal> calculateMinPrice(CryptoTrade v, SummaryTrade agg) {
        return extremeValuesCalculator.calculateExtremeValueRecord(v, agg, x -> x.getTrade().getPrice(),
                x -> x.getData().getMinPrice(), false);
    }


}
