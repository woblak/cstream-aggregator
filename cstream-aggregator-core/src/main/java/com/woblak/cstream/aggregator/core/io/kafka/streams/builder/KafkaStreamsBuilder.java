package com.woblak.cstream.aggregator.core.io.kafka.streams.builder;

import com.woblak.cstream.aggregator.core.io.kafka.streams.job.KafkaStreamJob;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;

import java.util.Properties;

public class KafkaStreamsBuilder {

    private StreamsBuilder streamsBuilder;
    private Properties props;
    private Thread.UncaughtExceptionHandler exceptionHandler;

    public KafkaStreamsBuilder(StreamsBuilder streamsBuilder, Properties props) {
        this.streamsBuilder = streamsBuilder;
        this.props= props;
    }

    public static KafkaStreamsBuilder withProps(Properties props) {
        return new KafkaStreamsBuilder(new StreamsBuilder(), props);
    }

    public KafkaStreamsBuilder addJob(KafkaStreamJob job) {
        this.streamsBuilder = job.registerOn(streamsBuilder);
        return this;
    }

    public KafkaStreamsBuilder onError(Thread.UncaughtExceptionHandler exceptionHandler) {
        this.exceptionHandler = exceptionHandler;
        return this;
    }

    public KafkaStreams build() {
        var kafkaStreams = new KafkaStreams(streamsBuilder.build(), props);
        if (exceptionHandler != null) {
            kafkaStreams.setUncaughtExceptionHandler(exceptionHandler);
        }
        return kafkaStreams;
    }
}
