package com.woblak.cstream.aggregator.core.util;

import lombok.experimental.UtilityClass;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.streams.kstream.TimeWindowedDeserializer;
import org.apache.kafka.streams.kstream.TimeWindowedSerializer;
import org.apache.kafka.streams.kstream.Windowed;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

@UtilityClass
public class SerdeBuilder {

    public static <T> Serde<T> create(Class<T> clazz) {
        return Serdes.serdeFrom(new JsonSerializer<>(),
                new JsonDeserializer<>(clazz).trustedPackages("*"));
    }

    public static <T> Serde<Windowed<T>> createWindowed(Serializer<T> serializer, Deserializer<T> deserializer) {
        Serializer<Windowed<T>> windowedSerializer = new TimeWindowedSerializer<>(serializer);
        Deserializer<Windowed<T>> windowedDeserializer = new TimeWindowedDeserializer<>(deserializer);
        return Serdes.serdeFrom(windowedSerializer, windowedDeserializer);
    }
}
