package com.woblak.cstream.aggregator.core.io.kafka.streams.job;

import org.apache.kafka.streams.kstream.KStream;

import java.util.List;

public interface WindowedStream<K1, V1, K2, V2> {

    List<KStream<K2, V2>> streamWindowed(KStream<K1, V1> kStream);
}
