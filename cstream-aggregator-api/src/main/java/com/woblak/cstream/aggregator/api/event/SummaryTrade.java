package com.woblak.cstream.aggregator.api.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@lombok.Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SummaryTrade {

    private String code;
    private String windowDurationName;

    private MetaData metaData;
    private Data data;

    @lombok.Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class MetaData {
        private long startMs;
        private long endMs;
    }

    @lombok.Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class Data {
        private AggregatedValue<Long> count;
        private AggregatedValue<BigDecimal> maxAmountQuote;
        private AggregatedValue<BigDecimal> minAmountQuote;
        private AggregatedValue<BigDecimal> minPrice;
        private AggregatedValue<BigDecimal> maxPrice;
    }
}
