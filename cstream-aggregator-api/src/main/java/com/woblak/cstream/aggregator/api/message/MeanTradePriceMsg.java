package com.woblak.cstream.aggregator.api.message;

import com.woblak.cstream.aggregator.api.event.MeanTradePrice;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;

@RequiredArgsConstructor
@Getter
@Builder
public class MeanTradePriceMsg implements Message<MeanTradePrice> {

    public static final String EVENT = "meanTrade-data";
    public static final String VERSION = "1.0";

    private final MessageHeaders headers;
    private final MeanTradePrice payload;
}
