package com.woblak.cstream.aggregator.api.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@lombok.Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MeanTradePrice {

    public static MeanTradePrice init() {
        return MeanTradePrice.builder()
                .metaData(MetaData.builder().build())
                .data(Data.builder()
                        .count(0L)
                        .amountSum(BigDecimal.ZERO)
                        .priceSum(BigDecimal.ZERO)
                        .priceWagedByAmountSum(BigDecimal.ZERO)
                        .mean(BigDecimal.ZERO)
                        .meanWagedByAmount(BigDecimal.ZERO)
                        .build())
                .build();
    }

    private String code;
    private String windowDurationName;

    private MetaData metaData;
    private Data data;

    @lombok.Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class MetaData {
        private long startMs;
        private long endMs;
    }

    @lombok.Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class Data {
        private Long count;
        private BigDecimal amountSum;
        private BigDecimal priceSum;
        private BigDecimal priceWagedByAmountSum;
        private BigDecimal mean;
        private BigDecimal meanWagedByAmount;
        private AggregatedValue<BigDecimal> maxPrice;
        private AggregatedValue<BigDecimal> minPrice;
    }
}
