package com.woblak.cstream.aggregator.api.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@lombok.Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AggregatedValue<V> {

    private String uuid;
    private Long timestamp;

    @NonNull
    private V value;
}