# cstream-aggregator

## About
Aggregates crypto trades.


## Run  
Building time is long because of tests of windowed aggregations.  

To run you need:  
- running Kafka on port given in yaml properties file  
- use specific profile for instance: `-Dspring.profiles.active=dev`